import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './core/route-guards/auth.guard';
import { RestauService } from './core/resolvers/restau.service';
import { AddRestauComponent } from './pages/add-restau/add-restau.component';
import { VoteGuard } from './core/route-guards/vote.guard';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},

  {path: 'login', component: LoginComponent},

  {path: 'home',
  loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
  resolve: {restaus: RestauService},
  canActivate: [AuthGuard, VoteGuard]},

  {path: 'vote-summary',
  loadChildren: () => import('./pages/vote-summary/vote-summary.module').then(m => m.VoteSummaryModule),
  canActivate: [AuthGuard]},

  {path: 'add-restau',
  loadChildren: () => import('./pages/add-restau/add-restau.module').then(m => m.AddRestauModule),
  canActivate: [AuthGuard]},
  
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
