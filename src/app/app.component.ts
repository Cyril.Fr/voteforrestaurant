import { Component, OnInit } from '@angular/core';
import { StoreDataService } from './core/services/store-data.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { USERS, RESTAUTS, INITIALIZE_VOTES, USER_VOTED_INITIALIZE } from './core/constants/constant';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'userReviews';
  currentRoute: string;

  constructor(private storeDataService: StoreDataService, private router: Router) {}

  ngOnInit(): void {

    // Store data at the launch of the app.component
    this.storeDataService.storedDataUser(USERS);
    this.storeDataService.storedDataRestau(RESTAUTS);
    this.storeDataService.storeData('userVoted', USER_VOTED_INITIALIZE);
    this.storeDataService.storeData('votes', INITIALIZE_VOTES);

    // get url after redirects
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.currentRoute = event.urlAfterRedirects;
      console.log('Url : ', event.urlAfterRedirects);
    });
  }
}
