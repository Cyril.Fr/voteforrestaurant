import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'src/app/core/services/session-storage.service';
import { VoteService } from 'src/app/core/services/vote.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { StoreDataService } from 'src/app/core/services/store-data.service';
import { toJson } from 'src/app/core/constants/constant';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  today: number = Date.now();
  public restaurants = toJson(localStorage.getItem('restaurants'));
  public votes = [];
  public userVoted = [];

  constructor(private sessionService: SessionStorageService,
              private voteService: VoteService,
              private authService: AuthService,
              private storeDataService: StoreDataService,
              private router: Router) { }

  ngOnInit() {
    
  }

  // method that verify if checkbox check and push restau in votes array
  // if checkbox uncheck return new votes array without this restaurant uncheck
  checkCheckBoxvalue(event, restau){
    console.log(event.checked, restau.name);
    if(event.checked === true) {
      this.votes.push(restau.name)
      console.log(this.votes);
    } else if(event.checked === false) {
      this.votes = this.votes.filter(rest => rest.name !== restau.name);
     console.log(this.votes);
    }
  }

  storeVote() {
    //this.sessionStorageService.storedVote(this.votes);
    this.voteService.userWhoVoted();
    if (this.sessionService.getData(this.authService.connectedUser.value.username).voted) {
      this.userVoted.push(this.sessionService.getData(this.authService.connectedUser.value.username));
      this.storeDataService.storeData('userVoted', [...toJson(localStorage.getItem('userVoted')), ...this.userVoted]);
    }
    this.voteService.add(this.votes);
    this.router.navigate(['/vote-summary'])
  }

}
