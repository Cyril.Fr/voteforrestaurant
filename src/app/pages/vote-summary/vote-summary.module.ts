import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { VoteSummaryComponent } from './vote-summary.component';
import { VoteSummaryRoutingModule } from './vote-summary-rooting.module';



@NgModule({
  declarations: [VoteSummaryComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    VoteSummaryRoutingModule
  ]
})
export class VoteSummaryModule { }
