import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VoteSummaryComponent } from './vote-summary.component';


const routes: Routes = [
  { path: '',  component: VoteSummaryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoteSummaryRoutingModule { }
