import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { SessionStorageService } from 'src/app/core/services/session-storage.service';
import { StoreDataService } from 'src/app/core/services/store-data.service';

@Component({
  selector: 'app-vote-summary',
  templateUrl: './vote-summary.component.html',
  styleUrls: ['./vote-summary.component.css']
})
export class VoteSummaryComponent implements OnInit {

  public votes = this.storeDataService.getData('votes');
  public votedUser = this.storeDataService.getData('userVoted');
  public users = this.apiService.getUsers();
  public stats = [];

  constructor(private apiService: ApiService, 
              private sessionStorageService: SessionStorageService,
              private storeDataService: StoreDataService) { }

  ngOnInit() {
    console.log('Number of user who voted : ', this.votedUser.length);
    console.log(this.voteStats());
  }

  // calcul le nombre de fois qu'un restau à été voté dans la liste de votes et retourne un tableau
  // contenant les résultats pour chaque restaurant
  voteStats() {
    let burgerKingVotes = this.votes.filter(vote => vote === 'Burger King').length
    let fratelliVotes = this.votes.filter(vote => vote === 'Fratelli Ristorante').length
    let gomexVotes = this.votes.filter(vote => vote === 'GOMEX').length
    let subwayVotes = this.votes.filter(vote => vote === 'SUBWAY').length
    return this.stats = [...[], {name: 'Burger King', num: burgerKingVotes },
                        {name: 'Fratelli Ristorante', num: fratelliVotes },
                        {name: 'GOMEX', num: gomexVotes },
                        {name: 'SUBWAY', num: subwayVotes }]
  }

}
