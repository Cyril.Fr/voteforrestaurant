import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { SessionStorageService } from 'src/app/core/services/session-storage.service';
import { INITIALIZE_VOTES } from 'src/app/core/constants/constant';
import { StoreDataService } from 'src/app/core/services/store-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private sessionStorageService: SessionStorageService,
              private storeDataService: StoreDataService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: []
    });
  }

  onSubmit() {
    console.log('username input : ', this.loginForm.get('username').value);

    console.log(this.authService.isUserExist(this.loginForm.get('username').value));
    // If username exist in local storage get username connected
    if (this.authService.isUsernameExist === true) {
      let userConnected = this.authService.userConnected(this.loginForm.get('username').value);
      this.sessionStorageService.storeUser(userConnected);
      this.sessionStorageService.storeData('votes', INITIALIZE_VOTES);
      this.router.navigate(['home']);
    }// else this.router.navigate(['login']);
    
    
  }

  

}
