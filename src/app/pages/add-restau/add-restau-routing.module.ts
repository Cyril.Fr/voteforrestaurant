import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddRestauComponent } from './add-restau.component';


const routes: Routes = [
  { path: '',  component: AddRestauComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddRestauRoutingModule { }
