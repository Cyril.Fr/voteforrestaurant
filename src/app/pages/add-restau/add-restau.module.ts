import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddRestauRoutingModule } from './add-restau-routing.module';
import { AddRestauComponent } from './add-restau.component';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AddRestauComponent],
  imports: [
    CommonModule,
    AddRestauRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [AddRestauComponent]
})
export class AddRestauModule { }
