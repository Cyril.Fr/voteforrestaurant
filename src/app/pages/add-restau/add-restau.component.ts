import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/core/services/api.service';
import { StoreDataService } from 'src/app/core/services/store-data.service';

@Component({
  selector: 'app-add-restau',
  templateUrl: './add-restau.component.html',
  styleUrls: ['./add-restau.component.css']
})
export class AddRestauComponent implements OnInit {

  createRestauForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private apiService: ApiService,
              private storeDataService: StoreDataService) { }

  ngOnInit() {
    this.createRestauForm = this.formBuilder.group({
      name: [],
      address: []
    })
  }

  onSubmit() {
    console.log(this.createRestauForm.value);
    let newRestausList = this.apiService.getRestaus().concat(this.createRestauForm.value);
    console.log(newRestausList);
    this.storeDataService.storedDataRestau(newRestausList);
  }

}
