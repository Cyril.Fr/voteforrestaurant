import { User } from '../models/user.model';
import { Restaurant } from '../models/restaurant.model';

export const USERS: User[] = [
    {
        username: 'Romain'
    },
    {
        username: 'Amine'
    },
    {
        username: 'Cyril'
    }
];

export const RESTAUTS: Restaurant[] = [
    {
        name: 'Burger King', address: 'Confluence'
    },
    {
        name: 'Fratelli Ristorante', address: '45 Quai Rambaud, Lyon'
    },
    {
        name: 'GOMEX', address: 'Confluence'
    },
    {
        name: 'SUBWAY', address: 'Confluence'
    }
]

export const INITIALIZE_VOTES: [] = [];

export const USER_VOTED_INITIALIZE: [] = [];


export function toJson(value: string) {
    return JSON.parse(value)
}

export function toString(value) {
    return JSON.stringify(value)
}