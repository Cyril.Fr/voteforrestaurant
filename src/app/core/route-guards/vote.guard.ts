import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { VoteService } from '../services/vote.service';
import { AuthService } from '../services/auth.service';
import { SessionStorageService } from '../services/session-storage.service';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class VoteGuard implements CanActivate {

  constructor(private voteService: VoteService,
              private authService: AuthService,
              private sessionService: SessionStorageService) {

  }
  canActivate(): boolean {
    if (this.sessionService.getData(this.authService.connectedUser.value.username) !== null && this.sessionService.getData(this.authService.connectedUser.value.username).voted) {
      return false
    } return true
  }

  
  
}
