import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { toJson } from '../constants/constant';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

constructor(private authService: AuthService, private router: Router) { }

  // protects access to the other page of the application as long as the user does not know authenticated
  canActivate(): boolean {
      /* if (this.authService.isUsernameExist === true) {
        return true;
      } this.router.navigate(['/login']); */
      if (toJson(sessionStorage.getItem('user')) !== null) {
        return true;
      } this.router.navigate(['/login']);
  }
  
}
