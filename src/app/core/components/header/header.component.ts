import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from '../../services/session-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,
              private sessionStorageService: SessionStorageService) { }

  ngOnInit() {
  }

  onClick(param: string) {
    console.log(param);
    this.router.navigate([param]);
  }

  logOut() {
    this.sessionStorageService.logOut();
  }

}
