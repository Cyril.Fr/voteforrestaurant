import { Injectable } from '@angular/core';
import { Restaurant } from '../models/restaurant.model';
import { SessionStorageService } from './session-storage.service';
import { AuthService } from './auth.service';
import { User } from '../models/user.model';
import { StoreDataService } from './store-data.service';

@Injectable({
  providedIn: 'root'
})
export class VoteService {

  constructor(private sessioStorageService: SessionStorageService,
              private storeDataService: StoreDataService,
              private authServcie: AuthService) { }

  // store vote in local storage
  add(votes) {
    if(!(votes.length == 0) && this.getVotes().length > 0) {
      this.storeDataService.storedVote(votes);
    } else this.storeDataService.storeData('votes', votes);
  }

  // get vote list from local storage
  getVotes(): Array<Restaurant> {
    return this.storeDataService.getData('votes');
  }

  // Store in session user who voted
  userWhoVoted() {
    let user;
    this.authServcie.connectedUser$.subscribe((connectedUser: User) =>{
      user = connectedUser
    });
    user = {...user, voted: true}
    this.sessioStorageService.storeData(user.username, user);
    this.authServcie.connectedUser.next(user);
  }
}
