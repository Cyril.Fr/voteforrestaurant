import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Restaurant } from '../models/restaurant.model';
import { toJson, toString } from '../constants/constant';

@Injectable({
  providedIn: 'root'
})
export class StoreDataService {

  constructor() {}

  // Store users in local storage
  storedDataUser(data: User[]) {
    localStorage.setItem('users', toString(data));
  }

  // Store restaurants in local storage
  storedDataRestau(data: Restaurant[]) {
    localStorage.setItem('restaurants', toString(data));
  }

  // Store any data in local storage
  storeData(key: string, value) {
    localStorage.setItem(key, toString(value));
  }

  // get any data from local storage
  getData(data: string) {
    return toJson(localStorage.getItem(data));
  }

  // Store vote list in local storage
  storedVote(voteData: Restaurant[]) {
    localStorage.setItem('votes', toString([...toJson(localStorage.getItem('votes')),...voteData]));
  }
}
