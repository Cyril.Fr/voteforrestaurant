import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Restaurant } from '../models/restaurant.model';
import { toJson } from '../constants/constant';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() { }

  // Get users from local storage
  getUsers(): User[] {
    return toJson(localStorage.getItem('users'));
  }

  // Get restaurants from local storage
  getRestaus(): Restaurant[] {
    return toJson(localStorage.getItem('restaurants'));
  }

}
