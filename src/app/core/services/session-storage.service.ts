import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { toJson, toString } from '../constants/constant';
import { Restaurant } from '../models/restaurant.model';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  constructor(private route: Router) { }

  // Store user in session storage
  storeUser(userLog: User) {
    sessionStorage.setItem('user', toString(userLog))
  }

  // Store data in session storage
  storeData(key: string, value) {
    sessionStorage.setItem(key, toString(value));
  }

  // Store vote list in session storage
  storedVote(voteData: Restaurant[]) {
    sessionStorage.setItem('votes', toString([...toJson(sessionStorage.getItem('votes')),...voteData]));
  }

  // Get vote from session storage
  getVote() {
    return toJson(sessionStorage.getItem('votes'));
  }

  getData(data: string) {
    return toJson(sessionStorage.getItem(data))
  }

  // Log out user in session and redirect to login page
  logOut() {
    sessionStorage.clear();
    this.route.navigate(['/login']);
  }
  
}
