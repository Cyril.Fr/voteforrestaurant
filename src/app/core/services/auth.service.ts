import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { ApiService } from './api.service';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isUsernameExist: boolean;
  public connectedUser: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public connectedUser$: Observable<any> =  this.connectedUser.asObservable()
  
  constructor(private apiService: ApiService,
              private authService: AuthService) { }

  // get connected User
  userConnected(userInput: String): User {
    
    this.apiService.getUsers().filter(user => user.username === userInput)
    .map(
      // val => console.log('user connected : ', val),
      val => this.connectedUser.next(val))
      return this.connectedUser.value
  }

  getConnectedUser(): User {
    let user: User;
    this.authService.connectedUser$.subscribe((connectedUser: User) => {
      user = connectedUser;
    });
    return user;
  }

  // Check if username intend to connect exists in local storage array of users
  isUserExist(userInput: String) {
      const resFilter = !!this.apiService.getUsers().filter(user => user.username === userInput).length;
      console.log('resFilter auth : ', resFilter);
      this.isUsernameExist = resFilter;
  }

}
