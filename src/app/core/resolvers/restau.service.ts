import { Injectable } from '@angular/core';
import { Restaurant } from '../models/restaurant.model';
import { Resolve } from '@angular/router';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class RestauService implements Resolve<Restaurant[]>{

  constructor(private apiService: ApiService) { }

  resolve(): Restaurant[] {
    return this.apiService.getRestaus();
  }
}
