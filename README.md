## Informations générales

Bienvenu dans l'application web vous permettant de voter pour votre restaurant préféré

## Fonctionnalités

Page de connection : pour vous connecter utiliser les 3 usernames reconnu par l'application (Romain, Cyril, Amine)

Page de vote : choisissez parmis la liste des restaurant celui que vous préférez, puis cliquez sur le bouton de vote en bas de la liste. Une fois voté vous êtes redirigé vers la page de résultat des votes et vous ne pouvez plus accéder à la page de vote.

Page de résultats des votes.

Page d'ajout de restaurant : vous pouvez ajouter un restaurant via le formulaire d'ajout en renseignez les champs

Déconnexion : pour vous déconnecter, cliquer sur le bouton log out dans le coin supérieur droit de la fenêtre


# UserReviews

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.25.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Auteur

Cyril, ingénieur d'étude.